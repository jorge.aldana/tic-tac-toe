package com.jorgealdana.triquiapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import com.jorgealdana.triquiapp.R
import com.jorgealdana.triquiapp.core.viewmodels.MainBoardViewModel

class MainBoard : Fragment(), LifecycleOwner {

    companion object {
        fun newInstance() = MainBoard()
    }

    private lateinit var viewModel: MainBoardViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main_board, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this)[MainBoardViewModel::class.java]
    }
}